<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Airportmodel extends CI_model{

	public function allAirports(){
		//To show all trips in the database
		$this->db->select('*')->from('airports')->order_by("UPPER(name_airport)");
		$query = $this->db->get();
		return $query->result();
	}

	public function editAirport($id_airport, $name_airport, $name_city){
		//To edit a airport

		//verify the data legality
		//$id_airport should be in table 'airports'
		$this->db->where('id_airport', $id_airport);
		$legal1 = $this->db->count_all_results('airports');

		//$name_airport should be a string with 3 characters
		$legal2 = (preg_match("/^[a-z]*$/i", $name_airport) AND strlen($name_airport) == 3);

		//$name_city should be a string with pure characters
		$legal3 = (preg_match("/^[a-z]*$/i", $name_city) AND !is_null($name_city));

		if (!$legal1){
			return array('error' => "Editing failed. The airport (first parameter $id_airport) has no match in the 'airports' table.");
		}
		elseif(!$legal2){
			return array('error' => "Editing failed. The airport name (second parameter $name_airport) should be 3 English characters.");
		}
		elseif(!$legal3){
			return array('error' => "Editing failed. The city (third parameter $name_city) should be pure English characters.");
		}
		else{
			$this->db->where('id_airport', $id_airport);
			$this->db->set('name_airport', strtoupper($name_airport));
			$this->db->set('name_city', ucwords($name_city));
			return $this->db->update('airports');
		}
	}	

	public function insertAirport($name_airport, $name_city){
		//To add a new airport to the database

		//verify the data legality
		//$name_airport should be a string with 3 characters
		$legal1 = (preg_match("/^[a-z]*$/i", $name_airport) AND strlen($name_airport) == 3);

		//$name_city should be a string with pure characters
		$legal2 = (preg_match("/^[a-z]*$/i", $name_city) AND !is_null($name_city));

		if (!$legal1){
			return array('error' => "Inserting failed. The airport name (first parameter $name_airport) should be 3 English characters.");
		}
		elseif(!$legal2){
			return array('error' => "Inserting failed. The city name (second parameter $name_city) should be pure English characters.");
		}
		else{
			$this->db->set('name_airport', strtoupper($name_airport));
			$this->db->set('name_city', ucwords($name_city));
			return $this->db->insert('airports');
		}
	}

	public function deleteAirport($id_airport){
		//To delete a trip from the database

		//verify the data legality
		//$id_airport need to be specified
		$legal1 = is_null($id_airport);

		//$id_airport should be in table 'airports'
		$this->db->where('id_airport', $id_airport);
		$legal2 = $this->db->count_all_results('airports');

		if ($legal1){
			return array('error' => "Deleting failed. You need to specify the airport id you want to delete.");
		}
		elseif (!$legal2){
			return array('error' => "Deleting failed. The airport (first parameter $id_airport) has no match in the 'airports' table.");
		}
		else {
			$this->db->where('id_airport', $id_airport);
			return $this->db->delete('airports');
		}
	}

	public function showAirport($id_airport){
		//To show a single trip from the database

		//verify the data legality
		//$id_airport need to be specified
		$legal1 = is_null($id_airport);
		
		//$id_airport should be in table 'airports'
		$this->db->where('id_airport', $id_airport);
		$legal2 = $this->db->count_all_results('airports');

		if ($legal1){
			return array('error' => "Showing failed. You need to specify the airport id you want to show.");
		}
		elseif (!$legal2){
			return array('error' => "Showing failed. The airport (first parameter $id_airport) has no match in the 'airports' table.");
		}
		else {
			$this->db->where('id_airport', $id_airport);
		    $query = $this->db->get('airports');
			return $query->result();
		}
	}
}
