<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flightmodel extends CI_model {

	public function allFlights(){
		//To show all flights in the database
		$this->db->select('id_flight, id_trip, A1.name_city AS from_city, A2.name_city AS to_city');
		$this->db->join('airports A1', 'A1.name_airport = flights.airport_from');
		$this->db->join('airports A2', 'A2.name_airport = flights.airport_to');
		$query = $this->db->get('flights');

		return $query->result();
	}

	public function editFlight($id_flight, $id_trip, $airport_from, $airport_to){
		//To edit a flight

		//verify the data legality
		//$id_flight should be in table 'flights'
		$this->db->where('id_flight', $id_flight);
		$legal1 = $this->db->count_all_results('flights');

		//$id_trip should be in table 'trips'
		$this->db->where('id_trip', $id_trip);
		$legal2 = $this->db->count_all_results('trips');

		//$airport_from and $airport_to should be in table 'airports'
		$this->db->where('name_airport', strtoupper($airport_from));
		$legal3 = $this->db->count_all_results('airports');
		$this->db->where('name_airport', strtoupper($airport_to));	
		$legal4 = $this->db->count_all_results('airports');

		if(!$legal1){
			return array('error' => "Editing failed. The flight (first parameter $id_flight) has no match in the 'flights' table.");
		} 
		elseif(!$legal2){
			return array('error' => "Editing failed. The trip (second parameter $id_trip) has no match in the 'trips' table.");
		}
		elseif(!$legal3){
			return array('error' => "Editing failed. The airport (third parameter $airport_from) has no match in the 'airports' table.");
		}
		elseif(!$legal4){
			return array('error' => "Editing failed. The airport (four parameter $airport_to) has no match in the 'airports' table.");
		}
		else {
		    $this->db->where('id_flight', $id_flight);
			$this->db->set('id_trip', $id_trip);
			$this->db->set('airport_from', strtoupper($airport_from));
			$this->db->set('airport_to', strtoupper($airport_to));
			return $this->db->update('flights');
		}
	}	

	public function insertFlight($id_trip, $airport_from, $airport_to){
		//To add a new flight to the database

		//verify the data legality
		//$id_trip should be in table 'trips'
		$this->db->where('id_trip', $id_trip);
		$legal1 = $this->db->count_all_results('trips');

		//$airport_from and $airport_to should be in table 'airports'
		$this->db->where('name_airport', strtoupper($airport_from));
		$legal2 = $this->db->count_all_results('airports');
		$this->db->where('name_airport', strtoupper($airport_to));	
		$legal3 = $this->db->count_all_results('airports');

		if(!$legal1){
			return array('error' => "Inserting failed. The trip (first parameter $id_trip) has no match in the 'trips' table.");
		}
		elseif(!$legal2){
			return array('error' => "Inserting failed. The depart airport (second parameter $airport_from) has no match in the 'airports' table.");
		}
		elseif(!$legal3){
			return array('error' => "Inserting failed. The arrival airport (thrid parameter $airport_to) has no match in the 'airports' table.");
		}
		else {
		    $this->db->set('id_trip', $id_trip);
			$this->db->set('airport_from', strtoupper($airport_from));
			$this->db->set('airport_to', strtoupper($airport_to));
			return $this->db->insert('flights');
		}
	}

	public function deleteFlight($id_flight){
		//To delete a flight from the database

		//verify the data legality
		//$id_airport need to be specified
		$legal1 = is_null($id_flight);

		//$id_flight should be in table 'flights'
		$this->db->where('id_flight', $id_flight);
		$legal2 = $this->db->count_all_results('flights');

		if ($legal1){
			return array('error' => "Deleting failed. You need to specify the flight id you want to delete.");
		}
		elseif (!$legal2){
			return array('error' => "Deleting failded. The flight (first parameter $id_flight) has no match in the 'flights' table.");
		}
		else {
		    $this->db->where('id_flight', $id_flight);
			return $this->db->delete('flights');
		}
	}

	public function showFlight($id_flight){
		//To show a single flight from the database

		//verify the data legality
		//$id_airport need to be specified
		$legal1 = is_null($id_flight);

		//$id_flight should be in table 'flights'
		$this->db->where('id_flight', $id_flight);
		$legal2 = $this->db->count_all_results('flights');

		if ($legal1){
			return array('error' => "Showing failed. You need to specify the flight id you want to show.");
		}
		elseif(!$legal2){
			return array('error' => "Showing failed. The flight (first parameter $id_flight) has no match in the 'flights' table.");
		}
		else {
		    $this->db->select('id_flight, id_trip, A1.name_city AS from_city, A2.name_city AS to_city')->from('flights');
			$this->db->join('airports A1', 'A1.name_airport = flights.airport_from');
			$this->db->join('airports A2', 'A2.name_airport = flights.airport_to');
			$this->db->where('id_flight', $id_flight);
			$query = $this->db->get();

			return $query->result();
		}
	}

	public function listFlights($id_trip){
		//To show all the flights in one trip.
		//verify the data legality
		//$id_trip need to be specified
		$legal1 = is_null($id_trip);

		//$id_trip should be in table 'trips'
		$this->db->where('id_trip', $id_trip);
		$legal2 = $this->db->count_all_results('trips');

		if ($legal1){
			return array('error' => "Showing failed. You need to specify the trip id you want to show.");
		}
		elseif (!$legal2){
			return array('error' => "Showing failed. The trip (first parameter $id_trip) has no match in the 'trips' table.");
			}
		else {
		    $this->db->select('id_flight, id_trip, A1.name_city AS from_city, A2.name_city AS to_city');
			$this->db->join('airports A1', 'A1.name_airport = flights.airport_from');
			$this->db->join('airports A2', 'A2.name_airport = flights.airport_to');
			$this->db->where('id_trip', $id_trip);
			$query = $this->db->get('flights');
			return $query->result();
		}
	}
}
