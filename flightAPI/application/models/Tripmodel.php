<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tripmodel extends CI_model{

	public function allTrips(){
		//To show all trips in the database
		$this->db->select('*')->from('trips')->order_by('UPPER(name_trip)');
		$query = $this->db->get();
		return $query->result();
	}

	public function editTrip($id_trip, $name_trip){
		//To rename a trip

		//Verify the data legality
		//$id_trip should be in table 'trips'
		$this->db->where('id_trip', $id_trip);
		$legal1 = $this->db->count_all_results('trips');

		//$name_trip should be specified
		$legal2 = is_null($name_trip);

		if(!$legal1){
			return array('error' => "Editing failed. The trip (first parameter $id_trip) has no match in the 'trips' table.");
		}
		elseif($legal2){
			return array('error' => "Editing failed. The trip name should not be empty.");
		}
		else{
			$this->db->where('id_trip', $id_trip);
			$this->db->set('name_trip', $name_trip);
			return $this->db->update('trips');
		}
	}	

	public function insertTrip($name_trip){
		//To add a new trip to the database

		//Verify the data legality
		//$name_trip need to be specified
		$legal1 = is_null($name_trip);
		if ($legal1){
			return array('error' => "Inserting failed. The trip name should not be empty.");
		}
		else{
			$this->db->set('name_trip', $name_trip);
			return $this->db->insert('trips');
		}
	}

	public function deleteTrip($id_trip){
		//To delete a trip from the database

		//verify the data legality
		//$id_trip need to be specified
		$legal1 = is_null($id_trip);

		//$id_trip should be in table 'trips'
		$this->db->where('id_trip', $id_trip);
		$legal2 = $this->db->count_all_results('trips');

		if ($legal1){
			return array('error' => "Deleting failed. You need to specify the trip id you want to delete.");
		}
		elseif(!$legal2){
			return array('error' => "Deleting failed. The trip (first parameter $id_trip) has no match in the 'trips' table.");
		}
		else{
			$this->db->where('id_trip', $id_trip);
			return $this->db->delete('trips');
		}
		
	}

	public function showTrip($id_trip){
		//To show a single trip from the database

		//verify the data legality
		//$id_trip need to be specified
		$legal1 = is_null($id_trip);

		//$id_trip should be in table 'trips'
		$this->db->where('id_trip', $id_trip);
		$legal2 = $this->db->count_all_results('trips');
		if ($legal1){
			return array('error' => "Showing failed. You need to specify the trip id you want to delete.");
		}
		elseif(!$legal2){
			return array('error' => "Showing failed. The trip (first parameter $id_trip) has no match in the 'trips' table.");
		}
		else{
			$this->db->where('id_trip', $id_trip);
			$query = $this->db->get('trips');
			return $query->result();
		}
	}
}
