CREATE DATABASE flightAPI;

USE flightAPI;

CREATE TABLE trips(
  id_trip INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_trip CHAR(30) NOT NULL DEFAULT ''
);

CREATE TABLE flights(
  id_flight INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_trip INT DEFAULT '0',
  airport_from CHAR(3) NOT NULL DEFAULT '',
  airport_to CHAR(3) NOT NULL DEFAULT ''
  /*FOREIGN KEY (id_trip) REFERENCES trips(id_trip)*/
);

CREATE TABLE airports(
  id_airport INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_airport CHAR(3) DEFAULT '',
  /*name_country CHAR(20) DEFAULT '',*/
  name_city CHAR(30) DEFAULT ''
);

INSERT INTO
  trips(name_trip)
  VALUES('My_trip_to_Germany'),
    ('My_trip_to_China'),
    ('My_trip_to_Thailand'),
    ('My_trip_around_the_world');

INSERT INTO
  flights(id_trip, airport_from, airport_to)
VALUES('1', 'YUL', 'SXF'),
  ('1', 'SXF', 'HAM'),
  ('1', 'XFW', 'YHU'),
  ('2', 'YUL', 'PEK'),
  ('2', 'PEK', 'PVG'),
  ('2', 'PVG', 'YUL'),
  ('3', 'YUL', 'DMK'),
  ('3', 'BKK', 'PYX'),
  ('3', 'PYX', 'YMX'),
  ('4', 'YHU', 'YKZ'),
  ('4', 'YTZ', 'PEK'),
  ('4', 'NAY', 'DMK'),
  ('4', 'BKK', 'PEK'),
  ('4', 'NAY', 'SXF'),
  ('4', 'THF', 'YUL');


INSERT INTO
  airports(name_airport, name_city)
VALUES('YUL', 'Montreal'),
  ('YHU', 'Montreal'),
  ('YMX', 'Montreal'),
  ('YKZ', 'Toronto'),
  ('YTZ', 'Toronto'),
  ('YYZ', 'Toronto'),
  ('PEK', 'Beijing'),
  ('NAY', 'Beijing'),
  ('PVG', 'Shanghai'),
  ('SHA', 'Shanghai'),
  ('SXF', 'Berlin'),
  ('THF', 'Berlin'),
  ('TXL', 'Berlin'),
  ('HAM', 'Hamburg'),
  ('XFW', 'Hamburg'),
  ('DMK', 'Bangkok'),
  ('BKK', 'Bangkok'),
  ('PYX', 'Pattaya');