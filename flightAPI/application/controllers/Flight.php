<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flight extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('flightmodel');
	}

	public function all(){
		//To show all flights in the database
		$all_flights = $this->flightmodel->allFlights();
		echo json_encode($all_flights);
	}

	public function edit($id_flight = null, $id_trip = null, $airport_from = null, $airport_to = null){
		//To rename a flight in the database
		$edited = $this->flightmodel->editFlight($id_flight, $id_trip, $airport_from, $airport_to);
		echo json_encode($edited);
	}

	public function insert($id_trip = null, $airport_from = null, $airport_to = null){
		//To add a new flight to the database
		$inserted = $this->flightmodel->insertFlight($id_trip, $airport_from, $airport_to);
		echo json_encode($inserted);
	}

	public function delete($id_flight = null){
		//To delete a flight from the database
		$deleted = $this->flightmodel->deleteFlight($id_flight);
		echo json_encode($deleted);
	}


	public function single($id_flight = null){
		//To show a flight from the database
		$showed = $this->flightmodel->showFlight($id_flight);
		echo json_encode($showed);
	}

	public function lists($id_trip = null){
		//To show all the flights in one trip
		$showed = $this->flightmodel->listFlights($id_trip);
		echo json_encode($showed);
	}
}