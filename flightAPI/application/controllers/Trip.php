<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trip extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('tripmodel');
	}

	public function all(){
		//To show all trips in the database
		$all_trips = $this->tripmodel->allTrips();
		echo json_encode($all_trips);
	}

	public function edit($id_trip = null, $name_trip = null){
		//To rename a trip in the database
		$renamed = $this->tripmodel->editTrip($id_trip, $name_trip);
		echo json_encode($renamed);
	}

	public function insert($name_trip = null){
		//To add a new trip to the database
		$inserted = $this->tripmodel->insertTrip($name_trip);
		echo json_encode($inserted);
	}

	public function delete($id_trip = null){
		//To delete a trip from the database
		$deleted = $this->tripmodel->deleteTrip($id_trip);
		echo json_encode($deleted);
	}

	public function single($id_trip = null){
		//To show a trip from the database
		$showed = $this->tripmodel->showTrip($id_trip);
		echo json_encode($showed);
	}

}