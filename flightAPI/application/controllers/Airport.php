<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Airport extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('airportmodel');
	}

	public function all(){
		//To show all airports in the database
		$all_airports = $this->airportmodel->allAirports();
		echo json_encode($all_airports);
	}

	public function edit($id_airport = null, $name_airport = null, $name_city = null){
		//To rename a airport in the database
		$renamed = $this->airportmodel->editAirport($id_airport, $name_airport, $name_city);
		echo json_encode($renamed);
	}

	public function insert($name_airport = null, $name_city = null){
		//To add a new airport to the database
		$inserted = $this->airportmodel->insertAirport($name_airport, $name_city);
		echo json_encode($inserted);
	}

	public function delete($id_airport = null){
		//To delete a airport from the database
		$deleted = $this->airportmodel->deleteAirport($id_airport);
		echo json_encode($deleted);
	}

	public function single($id_airport = null){
		//To show a airport from the database
		$showed = $this->airportmodel->showAirport($id_airport);
		echo json_encode($showed);
	}
}