# README #

This is the code for the **flightAPI** project, coded in PHP and Codeigniter (CI) framework.

The code is uploaded on bitbucket at https://bitbucket.org/wany1601/flightapi/.

The API is also deployed at http://flightapi.000webhostapp.com/. You can test it there.

## File list ##

This repository contains all the source code for the flightAPI, including:

* Source code of Codeigniter 3.1.1 (except for the 'user_guide' folder).
* A database file 'flightAPI.sql'.

## Installation ##

* LAMP environment 

**AppServ** (a integrated environment of Apache + PHP + MySQL)
AppServ can be downloaded at https://www.appserv.org/en/. Download and install it on the windows system (7, 8.1 and 10). it will automatically install all Apache, PHP and MySQL on your computer. A password may be asked to be set for MySQL while installing.

## Set up ##

1. Clone the code of this repository at the '\AppServ\www\'.
2. Modify the database config file at \AppServ\www\flightAPI\application\config\database.php. Change the password of MySQL as: 'password' => 'yourPassword'.
3. Create the flightAPI database by using '\AppServ\www\flightAPI\application\flightAPI.sql'.
4. The database can be viewed at: 'http://localhost/phpmyadmin/'.

## Function description ##

In this project, 3 controllers are built, namely 'Airport', 'Trip' and 'Flight'. The methods for each controller are listed as below:

### Airport ###
* all(): display all the airports.
* edit(id, name, city): edit a existing airport.
* insert(name, city): insert a new airport.
* delete(id): delete a airport.
* single(id): display a single airport.


### Trip ###
* all(): display all the trips.
* edit(id, name): edit a existing trip.
* insert(name): insert a new trip.
* delete(id): delete a trip.
* single(id): display a single trip.

### Flight ###
* all(): display all the flights.
* edit(id_flight, id_trip, airport_from, airport_to): edit a existing flight.
* insert(id_trip, airport_from, airport_to): insert a new flight.
* delete(id_flight): delete a flight.
* single(id_flight): show a single flight.
* lists(id_trip): list all the flight in one trip.

## How to Test ##
There are two ways to test the API:
* If you want to test the code on the <b>local server</b> set by AppServ, you can simply input the combination of the controller, method and parameters at the address bar of the browser with the form: http://localhost/flightapi/index.php/CONTROLLER/METHOD/PARAMETER1/PARAMETER2/PARAMETER3/.

Examples are given as below:

* Get the list of all airports (alphabetically):

http://localhost/flightapi/index.php/airport/all will return all the airports in the database.

* List all flights for a trip:

http://localhost/flightapi/index.php/flight/lists/1 will list all flights for trip with id = 1.

* Add a flight to a trip:

http://localhost/flightapi/index.php/flight/insert/4/yul/txl will insert a flight from YUL to TXL for trip with id = 4.

* Remove a flight from a trip:

http://localhost/flightapi/index.php/flight/delete/4/ will delete the flight with id = 4.

* Rename a trip:

http://localhost/flightapi/index.php/trip/edit/4/a_new_trip will rename the trip with id = 4 as 'a_new_trip'.

* If you want to test the code online at http://flightapi.000webhostapp.com/, you can simply input the combination of the controller, method and parameters at the address bar of the browser with the form: http://flightapi.000webhostapp.com/CONTROLLER/METHOD/PARAMETER1/PARAMETER2/PARAMETER3/.

Examples are given as below:

* Get the list of all airports (alphabetically):

http://flightapi.000webhostapp.com/airport/all will return all the airports in the database.

* List all flights for a trip:

http://flightapi.000webhostapp.com/flight/lists/1 will list all flights for trip with id = 1.

* Add a flight to a trip:

http://flightapi.000webhostapp.com/flight/insert/4/yul/txl will insert a flight from YUL to TXL for trip with id = 4.

* Remove a flight from a trip:

http://flightapi.000webhostapp.com/flight/delete/4/ will delete the flight with id = 4.

* Rename a trip:

http://flightapi.000webhostapp.com/trip/edit/4/a_new_trip will rename the trip with id = 4 as 'a_new_trip'.

## Future work ##

* Add 'country' to the 'airport' table. Since in reality, different cities in different countries may have the same name.
* Add a simple view to the project.